<?php

namespace zidivo\widgets\examples\actions;

use Yii;
use yii\base\Action;
use yii\bootstrap\ActiveForm;
use yii\web\Response;
use zidivo\widgets\examples\models\ExampleModel;

/**
 * Class MultipleInputAction
 * @package zidivo\widgets\examples\actions
 */
class MultipleInputAction extends Action
{
    public function run()
    {
        Yii::setAlias('@zidivo-examples', realpath(__DIR__ . '/../'));

        $model = new ExampleModel();

        $request = Yii::$app->getRequest();
        if ($request->isPost && $request->post('ajax') !== null) {
            $model->load(Yii::$app->request->post());
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validate($model);
            return $result;
        }

        if ($model->load(Yii::$app->request->post())) {
            if (!$model->validate()) {
                Yii::error('Validation errors: ' . print_r($model->getErrors(), true));
            }
        }
        
        return $this->controller->render('@zidivo-examples/views/multiple-input.php', ['model' => $model]);
    }
}