<?php

namespace zidivo\widgets\renderers;


/**
 * Interface RendererInterface
 * @package zidivo\widgets\renderers
 */
interface RendererInterface
{
    const POS_HEADER    = 'header';
    const POS_ROW       = 'row';
    const POS_FOOTER    = 'footer';

    /**
     * Renders the widget's content.
     *
     * @return mixed
     */
    public function render();

    /**
     * Set current context.
     * 
     * @param mixed $context
     * @return mixed
     */
    public function setContext($context);

    /**
     * Returns a placeholder.
     *
     * @return string
     */
    public function getIndexPlaceholder();
}