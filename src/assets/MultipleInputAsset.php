<?php

namespace zidivo\widgets\assets;

use yii\web\AssetBundle;

/**
 * Class MultipleInputAsset
 * @package zidivo\widgets\assets
 */
class MultipleInputAsset extends AssetBundle
{
    public $css = [
        'css/multiple-input.css'
    ];

    public $js = [];

    public $depends = [
        'yii\web\JqueryAsset'
    ];

    public function init()
    {
        $this->sourcePath = __DIR__ . '/src/';
        $this->js = [
            YII_DEBUG ? 'js/jquery.multipleInput.js' : 'js/jquery.multipleInput.min.js'
        ];
        parent::init();
    }


} 