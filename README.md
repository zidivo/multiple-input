#Zidivo Multiple input widget.
Yii2 widget for handle multiple inputs for an attribute of model and tabular input for batch of models.

##Installation
The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require  zidivo/multiple-input "~1.1"
```

or add

```
"zidivo/multiple-input": "~1.1"
```

to the require section of your `composer.json` file.

##Basic usage

![Single column example](./docs/images/single-column.gif?raw=true)

For example you want to have an ability of entering several emails of user on profile page.
In this case you can use multiple-input widget like in the following code

```php
use zidivo\widgets\MultipleInput;

...

<?php
    echo $form->field($model, 'emails')->widget(MultipleInput::className(), [
        'limit'             => 6,
        'allowEmptyList'    => false,
        'enableGuessTitle'  => true,
        'min'               => 2, // should be at least 2 rows
        'addButtonPosition' => MultipleInput::POS_HEADER // show add button in the header
    ])
    ->label(false);
?>
```

You can find more examples of usage [here](./docs/usage.md)

##Documentation

- [Configuration](./docs/configuration.md)
- [Usage](./docs/usage.md)
- [Tips and tricks](./docs/tips.md)
- [Javascript Events and Operations](./docs/javascript.md)
- [Renderers](./docs/renderers.md)

##License

Released under the BSD 3-Clause License. See the bundled [LICENSE.md](./LICENSE.md) for details.
